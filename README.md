## The Black Iron

The Official FoundryVTT system for The Black Iron: A Grimdark Fantasy RPG written by J Lasarde of FeralGamersInc Ltd. You can find the rulebook here: (https://preview.drivethrurpg.com/en/product/320941/the-black-iron-grimdark-fantasy-rpg)

This version of The Black Iron has been coded with permission for distribution from FeralGamersInc Ltd. It includes a character sheet, compendiums, and system support for The Black Iron.

All Images are used with permission or under Creative Commons licenses. Thank you Game-Icons.net, Mockaroon from Unsplash, Heritage Type Co., Clint Bellanger, Blarumyrran, CrowLineStudio, Justin Nichol, Scrittle, Varkalandar, and J Lasarde for your high-quality art. More info, including links and actual licenses, in the LICENSE.txt file.

Any questions, comments, concerns, etc. may be emailed to development.devin@gmail.com, communicated through gitlab, or communicated through Discord: https://discord.gg/ZvGfKPrRmy.

If you wish to support me as I maintain this system, you can buy me a coffee here! [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/F1F4G2J5X)

## Version 1.12.2

Update Includes:
- Fixed primary skill checkbox bug

## Version 1.12.1

Update Includes:
- v12 Compatibility
- Fixed courage roll bug
- Added Custom Skills Item

## Version 1.11.1

Update Includes:
- v11 Compatibility

## Version 1.10.2

Update Includes:
- Full v10 Migration

## Version 1.10.1

Update Includes:
- Initial v10 migration for immediate playability
- Updated Version Number system following FVTT Updates (v.1.10.1 will be the first update on v10, with 1.10.2 being the second and so-on).

## Version 1.0.0

The base system for The Black Iron.