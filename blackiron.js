import { blackiron } from "./module/config.js";
import * as Chat from "./module/chat.js";
import BlackironActor from "./module/BlackironActor.js";
import BlackironItem from "./module/BlackironItem.js";
import BlackironItemSheet from "./module/sheets/BlackironItemSheet.js";
import BlackironCharacterSheet from "./module/sheets/BlackironCharacterSheet.js";
import BlackironNpcSheet from "./module/sheets/BlackironNpcSheet.js";

Hooks.once("init", function () {
    console.log("blackiron | Initializing The Black Iron System");

    game.blackiron = {
        BlackironActor,
        BlackironItem
    }

    CONFIG.blackiron = blackiron;
    CONFIG.Actor.documentClass = BlackironActor;
    CONFIG.Item.documentClass = BlackironItem;

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("blackiron", BlackironItemSheet, { makeDefault: true });

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("blackiron", BlackironCharacterSheet, {
        types: ["character"],
        makeDefault: true
    });
    Actors.registerSheet("blackiron", BlackironNpcSheet, {
        types: ["npc"],
        makeDefault: true
    });
});

//Creating Chat Listeners
Hooks.on("renderChatLog", (app, html, data) => Chat.chatListeners(html));

//Adding a special Face for Dice So Nice
Hooks.on('diceSoNiceRollComplete', (chatMessageID) => {});

// Hooks.on('diceSoNiceRollStart', (id, context) => {
//   const roll = context.roll;
// )};

Hooks.once('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({ id: "blackiron", name: "Black Iron" }, "preferred");

    //Adding image to face 6
    dice3d.addDicePreset({
        type: "d6",
        labels: [
            '1','2','3','4','5','systems/blackiron/images/bi-face-6.png'
        ],
        colorset: "black",
        system: "blackiron"
    });
});