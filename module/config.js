export const blackiron = {};

/**
 * Quality Levels to be used for Items
 * @enum {string}
 */
blackiron.itemQualities = {
    none: "",
    lowQuality: "blackiron.qualityLow",
    goodQuality: "blackiron.qualityGood",
    highQuality: "blackiron.qualityHigh",
    masterworkQuality: "blackiron.qualityMasterwork"
};

/**
 * Damage Types to be used for Weapons
 * @enum {string}
 */
blackiron.damageTypes = {
    none: "",
    S: "blackiron.weaponSlashing",
    B: "blackiron.weaponBludgeon",
    P: "blackiron.weaponPiercing"
};

/**
 * Denotes Weapon type
 * @enum {string}
 */
blackiron.weaponType = {
    none: "",
    melee: "blackiron.weaponTypeMelee",
    ranged: "blackiron.weaponTypeRanged"
};

/**
 * Denotes how many Hands a Weapon requires
 * @enum {string}
 */
blackiron.weaponHands = {
    none: "",
    One: "blackiron.weaponOneHanded",
    Two: "blackiron.weaponTwoHanded",
    Versatile: "blackiron.weaponVersatile"
};

/**
 * Denotes Ammunition type
 * @enum {string}
 */
blackiron.ammunitionTypes = {
    none: "",
    arrows: "blackiron.ammunitionTypeArrows",
    bolts: "blackiron.ammunitionTypeBolts",
    stones: "blackiron.ammunitionTypeStones"
};

/**
 * Denotes Armour modifier numbers
 * @enum {number}
 */
blackiron.modifierNumbers = {
    negThree: -3,
    negTwo: -2,
    negOne: -1,
    zero: 0,
    posOne: +1,
    posTwo: +2
};

/**
 * Denotes Armour modified skills
 * @enum {string}
 */
blackiron.modifierSkills = {
    none: "",
    stealth: "Stealth",
    parry: "Parry"
};

/**
 * Denotes Skills list for Knacks
 * @enum {string}
 */
blackiron.knackSkills = {
    none: "",
    arcane: "blackiron.skillArcane",
    athletics: "blackiron.skillAthletics",
    crafting: "blackiron.skillCrafting",
    fight: "blackiron.skillFight",
    healing: "blackiron.skillHealing",
    lore: "blackiron.skillLore",
    melee: "blackiron.skillMelee",
    perception: "blackiron.skillPerception",
    performance: "blackiron.skillPerformance",
    ranged: "blackiron.skillRanged",
    scout: "blackiron.skillScout",
    stealth: "blackiron.skillStealth",
    survival: "blackiron.skillSurvival",
    social: "blackiron.skillSocial",
    thievery: "blackiron.skillThievery"
};

/**
 * Denotes School of Spell
 * @enum {string}
 */
blackiron.spellSchools = {
    none: "",
    necromancy: "blackiron.spellSchoolNecro",
    pyromancy: "blackiron.spellSchoolPyro",
    sorcery: "blackiron.spellSchoolSorc"
};

/**
 * Denotes Spell Type
 * @enum {string}
 */
blackiron.spellTypes = {
    touch: "blackiron.spellTypeTouch",
    ranged: "blackiron.spellTypeRanged",
    rangedAOE: "blackiron.spellTypeRangedAoE",
    aoe: "blackiron.spellTypeAoE",
    self: "blackiron.spellTypeSelf",
    ritual: "blackiron.spellTypeRitual"
};

/**
 * Denotes Potion or Salve
 * @enum {string}
 */
blackiron.potionTypes = {
    none: "",
    potion: "blackiron.potion",
    salve: "blackiron.potionSalve"
};

/**
 * Denotes Potion Use
 * @enum {string}
 */
blackiron.potionUses = {
    none: "",
    applied: "blackiron.potionUseApply",
    ingested: "blackiron.potionUseIngest"
};

/**
 * Denotes Custom Skill Attribute
 * @enum {string}
 */
blackiron.attr = {
    none: "",
    bod: "blackiron.attrBod",
    agl: "blackiron.attrAgl",
    mnd: "blackiron.attrMnd",
    ins: "blackiron.attrIns"
}