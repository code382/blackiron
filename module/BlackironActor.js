export default class BlackironActor extends Actor {
    prepareData() {
        super.prepareData();

        let actorData = this;
        let data = actorData.system;

        //getting derived data
        if (actorData.type === "character") this._prepareCharacterData(actorData);
        if (actorData.type === "npc") this._prepareNpcData(actorData);
    }

    _prepareCharacterData(actorData) {
        const data = actorData.system;

        //calculating wounds
        data.derived.wounds.max = (data.attributes.body * 5) + data.experience.addWound;

        //calculating stamina
        data.derived.stamina.max = (data.attributes.body * 3) + data.experience.addStam;

        //calculating courage
        if (data.attributes.instinct + data.attributes.mind == 10) {
            data.derived.courage = "3d6+5";
        } else if (data.attributes.instinct + data.attributes.mind >= 8) {
            data.derived.courage = "3d6+4";
        } else if (data.attributes.instinct + data.attributes.mind >= 6) {
            data.derived.courage = "3d6+3";
        } else if (data.attributes.instinct + data.attributes.mind >= 4) {
            data.derived.courage = "3d6+2";
        } else if (data.attributes.instinct + data.attributes.mind >= 2) {
            data.derived.courage = "3d6+1";
        }

        //calculating influence
        if (data.attributes.body + data.attributes.mind == 10) {
            data.derived.influence = +3;
        } else if (data.attributes.body + data.attributes.mind >= 8) {
            data.derived.influence = +2;
        } else if (data.attributes.body + data.attributes.mind >= 6) {
            data.derived.influence = +1;
        } else if (data.attributes.body + data.attributes.mind == 5) {
            data.derived.influence = 0;
        } else if (data.attributes.body + data.attributes.mind == 4) {
            data.derived.influence = -1;
        } else if (data.attributes.body + data.attributes.mind >= 2) {
            data.derived.influence = -2;
        }

        //calculating initiative
        if (data.attributes.instinct + data.attributes.agility == 10) {
            data.derived.initiative = 5 + data.experience.addInit;
        } else if (data.attributes.instinct + data.attributes.agility >= 8) {
            data.derived.initiative = 4 + data.experience.addInit;
        } else if (data.attributes.instinct + data.attributes.agility >= 6) {
            data.derived.initiative = 3 + data.experience.addInit;
        } else if (data.attributes.instinct + data.attributes.agility >= 4) {
            data.derived.initiative = 2 + data.experience.addInit;
        } else if (data.attributes.instinct + data.attributes.agility >= 2) {
            data.derived.initiative = 1 + data.experience.addInit;
        }
    }

    _prepareNpcData(actorData) {
        const data = actorData.system;

        //calculating wounds
        data.derived.wounds.max = data.attributes.body * 5;
        
        //calculating initiative
        if (data.attributes.instinct + data.attributes.agility == 10) {
            data.derived.initiative = 5;
        } else if (data.attributes.instinct + data.attributes.agility >= 8) {
            data.derived.initiative = 4;
        } else if (data.attributes.instinct + data.attributes.agility >= 6) {
            data.derived.initiative = 3;
        } else if (data.attributes.instinct + data.attributes.agility >= 4) {
            data.derived.initiative = 2;
        } else if (data.attributes.instinct + data.attributes.agility >= 2) {
            data.derived.initiative = 1;
        }
    }
}