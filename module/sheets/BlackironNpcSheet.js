import { attrRoll, skillRoll, weaponRoll } from "../blackironrolls.js";

export default class BlackironNpcSheet extends ActorSheet {

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            width: 800,
            height: 500,
            classes: ["blackiron", "sheet", "actor", "npc"]
        });
    }

    get template() {
        return `systems/blackiron/templates/sheets/actor/${this.actor.type}-sheet.html`;
    }

    async getData() {
        const baseData = super.getData();

        console.log("BlackironNpcSheet.js", "getData", { baseData });

        const sheetData = {
            owner: baseData.owner,
            editable: this.isEditable,
            actor: baseData.actor,
            data: baseData.actor.system,
            config: CONFIG.blackiron,
            weapon: baseData.items.filter(function(item) {return item.type == "weapon"}),
            knack: baseData.items.filter(function(item) {return item.type == "knack"}),
            armour: baseData.items.filter(function(item) {return item.type == "armour"}),
            ammunition: baseData.items.filter(function(item) {return item.type == "ammunition"}),
            spell: baseData.items.filter(function(item) {return item.type == "spell"}),
            wornGear: baseData.items.filter(function(item) {return item.type == "wornGear"}),
            carriedGear: baseData.items.filter(function(item) {return item.type == "carriedGear"}),
            potion: baseData.items.filter(function(item) {return item.type == "potion"}),
            poison: baseData.items.filter(function(item) {return item.type == "poison"}),
            disease: baseData.items.filter(function(item) {return item.type == "poison"}),
            enrichedNotes: await TextEditor.enrichHTML(this.actor.system.notes, {async: true})
        }

        console.log("BlackironNpcSheet.js", "getData", { sheetData });

        return sheetData;
    }

    activateListeners(html) {
        html.find(".item-create").click(this._onItemCreate.bind(this));
        html.find(".item-edit").click(this._onItemEdit.bind(this));
        html.find(".item-delete").click(this._onItemDelete.bind(this));
        html.find(".item-roll").click(this._onItemRoll.bind(this));
        html.find(".attr-roll").click(this._onAttrRoll.bind(this));
        html.find(".skill-roll").click(this._onSkillRoll.bind(this));
        html.find(".weapon-roll").click(this._onWeaponRoll.bind(this));

        super.activateListeners(html);
    }

    //Handle Owned Items
    _onItemCreate(event) {
        event.preventDefault();
        let element = event.currentTarget;

        let itemData = {
            name: "New Item",
            type: element.dataset.type
        };

        return this.actor.createEmbeddedDocuments("Item", [itemData]);
    }

    _onItemEdit(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;
        let item = this.actor.items.get(itemId);

        item.sheet.render(true);
    }

    _onItemDelete(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;

        return this.actor.deleteEmbeddedDocuments("Item", [itemId]);
    }

    _onItemRoll(event) {
        event.preventDefault();
        let itemID = event.currentTarget.closest(".item").dataset.itemId;
        const item = this.actor.items.get(itemID);

        item.roll();
    }

    //Handle Rolls from Sheet
    _onAttrRoll(event) {
        event.preventDefault();
        let element = event.currentTarget;
        attrRoll(element.dataset.attr, element.dataset.label);
    }

    _onSkillRoll(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let value = event.currentTarget.dataset.value;
        let label = event.currentTarget.dataset.label;
        
        //get attribute value
        let attrValue;
        switch (element.dataset.attr) {
            case "mind":
                attrValue = this.actor.system.attributes.mind;
                break;
            case "body":
                attrValue = this.actor.system.attributes.body;
                break;
            case "agility":
                attrValue = this.actor.system.attributes.agility;
                break;
            case "instinct":
                attrValue = this.actor.system.attributes.instinct;
                break;
            default: attrValue = null;
        }

        skillRoll(value, label, attrValue);
    }

    _onWeaponRoll(event){
        event.preventDefault();

        //get needed item data for function
        let itemID = event.currentTarget.closest(".item").dataset.itemId;
        const item = this.actor.items.get(itemID);
        let type = item.system.type;
        let mod = item.system.modifiers;
        let dmg = item.system.damage;
        let dmgType = item.system.damageType;
        let label = item.name;

        //get needed actor data
        let attrValue;
        let skillValue;
        if (type == "none") {
            attrValue = null;
            skillValue = null;
        } else if (type == "melee") {
            attrValue = this.actor.system.attributes.body;
            skillValue = this.actor.system.skills.melee.value;
        } else if (type == "ranged") {
            attrValue = this.actor.system.attributes.agility;
            skillValue = this.actor.system.skills.ranged.value;
        }

        weaponRoll(label, type, mod, dmg, dmgType, attrValue, skillValue);
    }
}