export default class BlackironItemSheet extends ItemSheet {

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            width: 530,
            height: 340,
            classes: ["blackiron", "sheet", "item"]
        });
    }

    get template () {
        return `systems/blackiron/templates/sheets/item/${this.item.type}-sheet.html`;
    }

    async getData() {
        const baseData = super.getData();
        let sheetData = {
            owner: this.item.isOwner,
            editable: this.isEditable,
            item: baseData.item,
            data: baseData.item.system,
            config: CONFIG.blackiron,
            enrichedNote: await TextEditor.enrichHTML(this.item.system.note, {async: true}),
            enrichedDescription: await TextEditor.enrichHTML(this.item.system.description, {async: true}),
            enrichedEffect: await TextEditor.enrichHTML(this.item.system.effect, {async: true})
        }

        return sheetData;
    }
}