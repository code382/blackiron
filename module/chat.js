export function chatListeners(html) {
    html.on('click', 'button.damage-roll', onDamageRoll);
    html.on('click', 'button.spell-roll', onSpellRoll);
}

function onDamageRoll(event) {
    //retrieve needed values from the button's dataset
    const stat = event.currentTarget.getAttribute('data-attr');
    let damage = event.currentTarget.getAttribute('data-dmg');
    const type = event.currentTarget.getAttribute('data-type');

    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollDamage(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollDamage(html) {
        //retrieve sitMod from Dialog
        let sitMod = html.find("input#sitMod").val();
        //assign other variables
        let content = "Damage Roll";
        let attrRollResult = 0;
        let firstDie = "1d6";
        let remainingDice = stat - 1;
        let rollFormula = `${stat}d6 + ${damage} + ${sitMod}`;
        let finalFormula = firstDie + "+" + `${remainingDice}d6 + ${damage}`;

        //roll the dice
        let r = await new Roll(finalFormula).evaluate({async: true});
        //customize for DSN
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //grab results and put into array for easier access
        let Result = [];
        for (let x of r.dice[0].results) {
            Result += x.result;
        }
        for (let x of r.dice[1].results) {
            Result += x.result;
        }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);
        console.log(resultArray);

        //grab Iron Die
        let ironDie = resultArray[0];
        console.log("Iron Die: " + ironDie);

        //find number of 6's rolled
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
            if (resultArray[i] == 6) {
                count6 = count6 + 1;
            }
        }
        console.log("# of 6's: " + count6);

        //get the final "result" of the roll
        let bonus;
        if (count6 >= 1) {
            bonus = count6 - 1;
            attrRollResult = 6 + bonus + +damage + parseInt(sitMod);
        } else {
            attrRollResult = Math.max(...resultArray) + +damage + parseInt(sitMod);
        }
        console.log("Attribute Roll Total: " + attrRollResult);

        /** check if Iron Die is a 1 or 6 and create custom message for chat
         *  also change damage to match crit or fumble
         *  also change attrRollResult to only damage if fumble
         */
        let ironMessage = "";
        if (ironDie == 6) {
            damage = damage * 2;
            attrRollResult = 6 + bonus + +damage + parseInt(sitMod);
            ironMessage = `<b>CRITICAL HIT</b>: The Iron Die has rolled a six. Your weapon damage was doubled to ${damage}`;
        } else if (ironDie == 1) {
            damage = damage / 2;
            attrRollResult = damage;
            ironMessage = `<b>FUMBLE</b>: The Iron Die has rolled a one. No damage comes from the dice, and your weapon damage was halved to ${damage}. This weapon loses one break level.`;
        }

        //check if the final "result" means max damage and adds an effect
        let weaponEffect = "";
        if (count6 == resultArray.length && type == null) {
            weaponEffect = "MAX DAMAGE: You've rolled maximum damage, but you don't have a damage type set. Apply the effect that suits your weapon's damage type.";
        } else if (count6 == resultArray.length && type == "S") {
            weaponEffect = "MAX DAMAGE: You've rolled maximum damage and your opponent is now bleeding. They lose 1 Stamina per round until the bleeding is staunched.";
        } else if (count6 == resultArray.length && type == "P") {
            weaponEffect = "MAX DAMAGE: You've rolled maximum damage and your opponent is now bleeding. They lose 1 Stamina per round until the bleeding is staunched.";
        } else if (count6 == resultArray.length && type == "B") {
            weaponEffect = "MAX DAMAGE: You've rolled maximum damage and your opponent is now stunned for [[1d3]] rounds. They will be unable to attack and will be at a -2 to parry, block, or dodge.";
        }

        //finally, post to chat
        r.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: content,
            content: `<div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Damage Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <div>${ironMessage}</div>
                    <hr>
                    <div>${weaponEffect}</div>
                </div>
            </div>`
        });
    }
}

function onSpellRoll(event) {
    //get spell and caster info
    const card = event.currentTarget.closest(".spell");
    let caster = game.actors.get(card.dataset.ownerId);
    let spell = caster.items.get(card.dataset.itemId);

    //retrieve appropriate stats
    let stat = caster.system.attributes.mind;
    let skillMod = caster.system.skills.arcane.value;
    let tn = spell.system.tn;
    let oneSuccess = spell.system.degreeSuccess.degOne;
    let twoSuccess = spell.system.degreeSuccess.degTwo;
    let threeSuccess = spell.system.degreeSuccess.degThree;
    let oneFail = spell.system.degreeFail.degOne;
    let twoFail = spell.system.degreeFail.degTwo;
    let threeFail = spell.system.degreeFail.degThree;

    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollSpell(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollSpell(html) {
        //get sitMod from Dialog
        let sitMod = html.find("input#sitMod").val();

        //get roll formula based on having or not having skill
        let firstDie = "1d6";
        let remainingDice = stat - 1;
        let rollFormula;
        let finalFormula;
        if (skillMod == 0) {
            rollFormula = `${stat}d6kl + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6`;
        } else {
            rollFormula = `${stat}d6 + ${skillMod} + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6 + ${skillMod}`;
        }

        //roll the dice
        let r = await new Roll(finalFormula).evaluate({async: true});
        //customize for DSN
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //grab results and put into array for easier access
        let Result = [];
        for (let x of r.dice[0].results) {
            Result += x.result;
        }
        for (let x of r.dice[1].results) {
            Result += x.result;
        }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);
        console.log(resultArray);

        //grab Iron Die
        let ironDie = resultArray[0];
        console.log("Iron Die: " + ironDie);

        //check if Iron Die is a 1 or 6 and create custom message for chat
        let ironMessage = "";
        if (ironDie == 6) {
            ironMessage = "Luck is with you. The Iron Die has rolled a 6.";
        } else if (ironDie == 1) {
            ironMessage = "This world is without mercy. The Iron Die has rolled a 1."
        }

        //find number of 6's rolled
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
            if (resultArray[i] == 6) {
                count6 = count6 + 1;
            }
        }
        console.log("# of 6's: " + count6);

        //get the final "result" of the roll based on if skilled
        let attrRollResult;
        if (skillMod == 0) {
            attrRollResult = Math.min(...resultArray) + parseInt(sitMod);
        } else {
            if (count6 >= 1) {
                let bonus = count6 - 1;
                attrRollResult = 6 + +bonus + +skillMod + parseInt(sitMod);
            } else {
                let maxResult = Math.max(...resultArray);
                attrRollResult = +maxResult + +skillMod + parseInt(sitMod);
            }
        }
        console.log("Attribute Roll Total: " + attrRollResult);

        //get level of success or failure
        let levelMessage;
        if (attrRollResult >= (+tn + 6)) {
            levelMessage = `Three Levels of Success: </hr> ${threeSuccess}`;
        } else if (attrRollResult >= (+tn + 4)) {
            levelMessage = `Two Levels of Success: </hr> ${twoSuccess}`;
        } else if (attrRollResult >= (+tn + 2)) {
            levelMessage = `One Level of Success: </hr> ${oneSuccess}`;
        } else if (attrRollResult >= tn) {
            levelMessage = "The spell was cast successfully.";
        } else if (attrRollResult >= (+tn - 1)) {
            levelMessage = "The spell has failed.";
        } else if (attrRollResult >= (+tn - 3)) {
            levelMessage = `One Level of Failure: </hr> ${oneFail}`;
        } else if (attrRollResult >= (+tn - 5)) {
            levelMessage = `Two Levels of Failure: </hr> ${twoFail}`;
        } else if (attrRollResult <= (+tn - 6)) {
            levelMessage = `Three Levels of Failure: </hr> ${threeFail}`;
        }

        //finally, post to chat based on if skilled or not
        let content = `Casting ${spell.name}`;
        if (skillMod == 0) {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Roll Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <div>You are unskilled in this area. Take the lowest roll.</div>
                    <hr>
                    <div>${ironMessage}</div>
                    <hr>
                    <div>${levelMessage}</div>
                </div>
                </div>`
            });
        } else {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                        <table>
                            <tr>
                                <th style="font-family: Deutsch;">Iron Die: </th>
                                <th style="font-family: Deutsch;">Roll Total: </th>
                            </tr>
                            <tr>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                                </td>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                                </td>
                            </tr>
                        </table>
                        <div>${ironMessage}</div>
                        <hr>
                        <div>${levelMessage}</div>
                    </div>
                </div>`
            });
        }
    }
}