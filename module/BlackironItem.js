export default class BlackironItem extends Item {
    chatTemplate = {
        "weapon": "systems/blackiron/templates/chat/weapon-chat.hbs",
        "armour": "systems/blackiron/templates/chat/armour-chat.hbs",
        "ammunition": "systems/blackiron/templates/chat/ammunition-chat.hbs",
        "carriedGear": "systems/blackiron/templates/chat/carriedGear-chat.hbs",
        "disease": "systems/blackiron/templates/chat/disease-chat.hbs",
        "knack": "systems/blackiron/templates/chat/knack-chat.hbs",
        "poison": "systems/blackiron/templates/chat/poison-chat.hbs",
        "potion": "systems/blackiron/templates/chat/potion-chat.hbs",
        "spell": "systems/blackiron/templates/chat/spell-chat.hbs",
        "wornGear": "systems/blackiron/templates/chat/wornGear-chat.hbs"
    };

    prepareDerivedData() {
        super.prepareDerivedData();

        const itemData = this;
        const data = itemData.system;

        if (itemData.type === "weapon") {
            if (data.quality === "none") {
                data.modifiers = null;
            } else if (data.quality === "lowQuality") {
                data.modifiers = -2;
            } else if (data.quality === "goodQuality") {
                data.modifiers = -1;
            } else if (data.quality === "highQuality") {
                data.modifiers = 0;
            } else {
                data.modifiers = +1;
            }
        }

        if (itemData.type === "knack") {
            if (data.skill === "arcane" ) {
                data.attr = "mind";
            } else if (data.skill === "athletics" ) {
                data.attr = "body";
            } else if (data.skill === "crafting" ) {
                data.attr = "agility";
            } else if (data.skill === "fight" ) {
                data.attr = "body";
            } else if (data.skill === "healing" ) {
                data.attr = "mind";
            } else if (data.skill === "lore" ) {
                data.attr = "mind";
            } else if (data.skill === "melee" ) {
                data.attr = "body";
            } else if (data.skill === "perception" ) {
                data.attr = "instinct";
            } else if (data.skill === "performance" ) {
                data.attr = "mind";
            } else if (data.skill === "ranged" ) {
                data.attr = "agility";
            } else if (data.skill === "scout" ) {
                data.attr = "instinct";
            } else if (data.skill === "stealth" ) {
                data.attr = "agility";
            } else if (data.skill === "survival" ) {
                data.attr = "instinct";
            } else if (data.skill === "social" ) {
                data.attr = "mind";
            } else if (data.skill === "thievery" ) {
                data.attr = "agility";
            } else {
                data.attr = null;
            }
        }
    }

    async roll() {
        let chatData = {
            user: game.user._id,
            speaker: ChatMessage.getSpeaker()
        };

        let cardData = {
            ...this,
            id: this.id,
            owner: this.actor.id
        };

        chatData.content = await renderTemplate(this.chatTemplate[this.type], cardData);

        return ChatMessage.create(chatData);
    }
}