export async function injuryRoll (dice) {
    let content = "";
    let formula = dice;
    let r = await new Roll("@formula", {formula}).evaluate();
    r.dice[0].options.appearance = {
        colorset:"custom",
        foreground: "#000000",
        background: "#FFFFFF",
        outline: "#FFFFFF",
        edge: "#FFFFFF"
    };

    if (r.result == 12) {
        content = `<html>
            <h1>Rolling for Injury</h1>
            <h3>Injury Sustained: Head</h3>
            </html>`
    } else if (r.result >= 10) {
        content = `<html>
        <h1>Rolling for Injury</h1>
        <h3>Injury Sustained: Right Arm</h3>
        </html>`
    } else if (r.result >= 8) {
        content = `<html>
        <h1>Rolling for Injury</h1>
        <h3>Injury Sustained: Body</h3>
        </html>`
    } else if (r.result >= 6) {
        content = `<html>
        <h1>Rolling for Injury</h1>
        <h3>Injury Sustained: Left Arm</h3>
        </html>`
    } else if (r.result >= 4) {
        content = `<html>
        <h1>Rolling for Injury</h1>
        <h3>Injury Sustained: Right Leg</h3>
        </html>`
    } else if (r.result >= 2) {
        content = `<html>
        <h1>Rolling for Injury</h1>
        <h3>Injury Sustained: Left Leg</h3>
        </html>`
    }

    r.toMessage({
        speaker: ChatMessage.getSpeaker(),
        flavor: content
    });
}

export function attrRoll (attr, label) {
    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollAttr(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollAttr(html) {
        //assign initial stats
        let sitMod = html.find("input#sitMod").val();
        let content = `${label} Roll`;
        let stat = attr;
        let rollFormula = `${stat}d6 + ${sitMod}`;
        let attrRollResult = 0;
        let firstDie = "1d6";
        let remainingDice = stat - 1;
        let finalFormula = firstDie + "+" + `${remainingDice}d6`;

        //roll the dice
        let r = await new Roll(finalFormula).evaluate();
        //customize for DSN
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //grab results and put into array for easier access
        let Result = [];
        for (let x of r.dice[0].results) {
            Result += x.result;
        }
        for (let x of r.dice[1].results) {
            Result += x.result;
        }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);
        console.log(resultArray);

        //grab Iron Die
        let ironDie = resultArray[0];
        console.log("Iron Die: " + ironDie);

        //check if Iron Die is a 1 or 6 and create custom message for chat
        let ironMessage = "";
        if (ironDie == 6) {
            ironMessage = "Luck is with you. The Iron Die has rolled a 6.";
        } else if (ironDie == 1) {
            ironMessage = "This world is without mercy. The Iron Die has rolled a 1."
        }

        //find number of 6's rolled
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
            if (resultArray[i] == 6) {
                count6 = count6 + 1;
            }
        }
        console.log("# of 6's: " + count6);

        //get the final "result" of the roll
        if (count6 >= 1) {
            let bonus = count6 - 1;
            attrRollResult = 6 + bonus + parseInt(sitMod);
        } else {
            let maxResult = Math.max(...resultArray);
            attrRollResult = parseInt(maxResult) + parseInt(sitMod);
        }
        console.log("Attribute Roll Total: " + attrRollResult);

        //finally, post to chat
        r.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: content,
            content: `<div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Roll Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <div>${ironMessage}</div>
                </div>
            </div>`
        });
    }
}

export function skillRoll (value, label, attrValue) {
    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollSkill(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollSkill(html) {
        //assign initial variables
        let sitMod = html.find("input#sitMod").val();
        let content = `<span style="text-transform: capitalize;">${label}</span> Roll`;
        let stat = attrValue;
        let skillMod = value;
        let attrRollResult = 0;
        let firstDie = "1d6";
        let remainingDice = stat - 1;
        let finalFormula;

        //get roll formula based on having or not having skill
        let rollFormula;
        if (skillMod == 0) {
            rollFormula = `${stat}d6kl + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6`;
        } else {
            rollFormula = `${stat}d6 + ${skillMod} + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6 + ${skillMod}`;
        }
        console.log(finalFormula);

        //roll the dice
        let r = await new Roll(finalFormula).evaluate();
        //customize for DSN
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //grab results and put into array for easier access
        let Result = [];
        for (let x of r.dice[0].results) {
            Result += x.result;
        }
        for (let x of r.dice[1].results) {
            Result += x.result;
        }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);
        console.log(resultArray);

        //grab Iron Die
        let ironDie = resultArray[0];
        console.log("Iron Die: " + ironDie);

        //check if Iron Die is a 1 or 6 and create custom message for chat
        let ironMessage = "";
        if (ironDie == 6) {
            ironMessage = "Luck is with you. The Iron Die has rolled a 6.";
        } else if (ironDie == 1) {
            ironMessage = "This world is without mercy. The Iron Die has rolled a 1."
        }

        //find number of 6's rolled
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
            if (resultArray[i] == 6) {
                count6 = count6 + 1;
            }
        }
        console.log("# of 6's: " + count6);

        //get the final "result" of the roll
        if (skillMod == 0) {
            attrRollResult = Math.min(...resultArray) + parseInt(sitMod);
        } else {
            if (count6 >= 1) {
                let bonus = count6 - 1;
                attrRollResult = 6 + +bonus + +skillMod + parseInt(sitMod);
            } else {
                let maxResult = Math.max(...resultArray);
                attrRollResult = +maxResult + +skillMod + parseInt(sitMod);
            }
        }
        console.log("Attribute Roll Total: " + attrRollResult);

        //finally, post to chat based on if skilled or not
        if (skillMod == 0) {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Roll Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <div>You are unskilled in this area. Take the lowest roll.</div>
                    <div>${ironMessage}</div>
                </div>
                </div>`
            });
        } else {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                        <table>
                            <tr>
                                <th style="font-family: Deutsch;">Iron Die: </th>
                                <th style="font-family: Deutsch;">Roll Total: </th>
                            </tr>
                            <tr>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                                </td>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                                </td>
                            </tr>
                        </table>
                        <div>${ironMessage}</div>
                    </div>
                </div>`
            });
        }
    }
}

export function weaponRoll (label, type, mod, dmg, dmgType, attrValue, skillValue) {
    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollWeapon(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollWeapon(html) {   
        //assign initial variables
        let sitMod = html.find("input#sitMod").val();
        let content = `<span style="text-transform: capitalize;">${label}</span> Attack`;
        let weaponType = type;
        let toHit = mod;
        let damage = dmg;
        let damageType = dmgType;
        let stat = attrValue;
        let skillMod = skillValue;
        let attrRollResult = 0;
        let firstDie = "1d6";
        let remainingDice = stat - 1;

        //get roll formula based on having or not having skill
        let rollFormula;
        let finalFormula;
        if (skillMod == 0) {
            rollFormula = `${stat}d6kl + ${toHit} + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6 + ${toHit}`;
        } else {
            rollFormula = `${stat}d6 + ${skillMod} + ${toHit} + ${sitMod}`;
            finalFormula = firstDie + "+" + `${remainingDice}d6 + ${skillMod} + ${toHit}`;
        }

        //roll the dice
        let r = await new Roll(finalFormula, {toHit}).evaluate();
        //customize for DSN
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //grab results and put into array for easier access
        let Result = [];
        for (let x of r.dice[0].results) {
            Result += x.result;
        }
        for (let x of r.dice[1].results) {
            Result += x.result;
        }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);
        console.log(resultArray);

        //grab Iron Die
        let ironDie = resultArray[0];
        console.log("Iron Die: " + ironDie);

        //check if Iron Die is a 1 or 6 and create custom message for chat
        let ironMessage = "";
        if (ironDie == 6) {
            ironMessage = "Luck is with you. The Iron Die has rolled a 6.";
        } else if (ironDie == 1) {
            ironMessage = "This world is without mercy. The Iron Die has rolled a 1."
        }

        //find number of 6's rolled
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
            if (resultArray[i] == 6) {
                count6 = count6 + 1;
            }
        }
        console.log("# of 6's: " + count6);

        //get the final "result" of the roll
        if (skillMod == 0) {
            let minResult = Math.min(...resultArray);
            attrRollResult = +minResult + +toHit + parseInt(sitMod);
        } else {
            if (count6 >= 1) {
                let bonus = count6 - 1;
                attrRollResult = 6 + +bonus + +skillMod + +toHit + parseInt(sitMod);
            } else {
                let maxResult = Math.max(...resultArray);
                attrRollResult = +maxResult + +skillMod + +toHit + parseInt(sitMod);
            }
        }
        console.log("Attribute Roll Total: " + attrRollResult);

        //finally, post to chat based on if skilled or not
        if (skillMod == 0) {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                <div class="dice-result">
                    <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Roll Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <button class="damage-roll" data-attr="${stat}" data-dmg="${damage}" data-type="${damageType}">Roll Damage</button>
                    <div>You are unskilled in this area. Take the lowest roll.</div>
                    <div>${ironMessage}</div>
                </div>
                </div>`
            });
        } else {
            r.toMessage({
                speaker: ChatMessage.getSpeaker(),
                flavor: content,
                content: `<div class="dice-roll">
                    <div class="dice-result">
                        <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                        <table>
                            <tr>
                                <th style="font-family: Deutsch;">Iron Die: </th>
                                <th style="font-family: Deutsch;">Roll Total: </th>
                            </tr>
                            <tr>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                                </td>
                                <td>
                                    <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                                </td>
                            </tr>
                        </table>
                        <button class="damage-roll" data-attr="${stat}" data-dmg="${damage}" data-type="${damageType}">Roll Damage</button>
                        <div>${ironMessage}</div>
                    </div>
                </div>`
            });
        }
    }
}

export async function courageRoll (value) {
    //start everything off with a Dialog to obtain situational modifier
    const sitModDialog = new Dialog({
        title: "Situational Modifier",
        content: `
            <html>
            <table class="section-table>
            <tr>
                <td><label for="sitMod">Situational Modifier</label></td>
                <td><input class="sitMod" type="text" id="sitMod" placeholder="e.g.: -1 or +2" value="0"></td>
            </tr>
            </table>
            </html>`,
        buttons: {
            button1: {
                label: "Roll",
                callback: (html) => rollCourage(html)
            }
        },
        default: "button1"
    }).render(true);

    async function rollCourage(html) {
        //getting sitMod from Dialog
        let sitMod = html.find("input#sitMod").val();

        //getting mod for roll
        let courage = value;
        let mod;
        switch (courage) {
            case "3d6+5": mod = 5; break;
            case "3d6+4": mod = 4; break;
            case "3d6+3": mod = 3; break;
            case "3d6+2": mod = 2; break;
            case "3d6+1": mod = 1; break;
            default: mod = 0; }
        console.log(courage, mod);

        //getting roll formula and label
        let content = "Rolling Courage";
        let rollFormula = courage + "+" + sitMod;

        //roll the dice, with styling for Dice So Nice
        let r = await new Roll(`1d6 + 2d6 + ${mod}`, {mod}).evaluate();
        r.dice[0].options.appearance = {
            colorset:"custom",
            foreground: "#FFFFFF",
            background: "#000000",
            outline: "#000000",
            edge: "#000000"
        };
        r.dice[1].options.appearance = {
            colorset:"custom",
            foreground: "#000000",
            background: "#FFFFFF",
            outline: "#FFFFFF",
            edge: "#FFFFFF"
        };

        //throw everything in my own array
        let Result = [];
        for (let x of r.dice[0].results) { Result += x.result; }
        for (let x of r.dice[1].results) { Result += x.result; }
        let myFunc = num => Number(num);
        let resultArray = Array.from(String(Result), myFunc);

        //separate the Iron Die and add its respective messages
        let ironDie = resultArray[0];
        let ironMessage = "";
        switch (ironDie) {
            case 6: ironMessage = "Luck is with you. The Iron Die has rolled a 6."; break;
            case 1: ironMessage = "This world is without mercy. The Iron Die has rolled a 1."; break;
            default: ironMessage = ""; }

        //count successes and add accordingly
        let count6 = 0;
        for (let i=0; i<resultArray.length; i++) {
        if (resultArray[i] == 6) { count6 = count6 + 1; }
        }

        let attrRollResult;
        if (count6 >= 1) {
            let bonus = count6 - 1;
            attrRollResult = 6 + bonus + +mod + parseInt(sitMod);
        } else {
            attrRollResult = Math.max(...resultArray) + +mod + parseInt(sitMod);
        }

        //and finally sending to chat
        r.toMessage({
            speaker: ChatMessage.getSpeaker(),
            flavor: content,
            content: `<div class="dice-roll">
                <div class="dice-result">
                <div class="dice-formula" title="${resultArray}">${rollFormula}</div>
                    <table>
                        <tr>
                            <th style="font-family: Deutsch;">Iron Die: </th>
                            <th style="font-family: Deutsch;">Roll Total: </th>
                        </tr>
                        <tr>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${ironDie}</h4>
                            </td>
                            <td>
                                <h4 class="dice-total" style="width: 25%;">${attrRollResult}</h4>
                            </td>
                        </tr>
                    </table>
                    <div>${ironMessage}</div>
                </div>
                </div>`
        });
    }
}